
const content = document.querySelector('.content'); 
const startButton = document.querySelector('#start'); 
const workingTimeDisplay = document.querySelector('#timeworking'); 
const pauseTimeDisplay = document.querySelector('#timepausing'); 
const workingTimeStats = document.querySelector('#workingtimestats'); 

let currentlyWorking = 0; 
let workingTimeStart = 0; 
let workingTimeTotal = 0; 
let pauseTime = 0; 
let pauseTimeTotal = 0; 
let ratio = 1/3; 
let timeElapsed = 0; 

function startClock() { 

    if (currentlyWorking) {
        startButton.textContent = "Start"; 
        startButton.style.cssText = "background-color: lightgreen;"
        currentlyWorking = 0; 
        workingTimeTotal += timeElapsed/1000;
        workingTimeStats.textContent = `Working Time Total: ${Math.floor(workingTimeTotal/3600)} hours, ${Math.floor(workingTimeTotal/60)%60} minutes, ${Math.floor(workingTimeTotal%60)} seconds`

    }
    else {
        startButton.textContent = "Stop"; 
        startButton.style.cssText = "background-color: lightcoral;"
        currentlyWorking = 1; 
        workingTimeStart = new Date(); 
    }
}

function updateWorkingTime() {
    if (currentlyWorking) {
        const now = new Date(); 
        timeElapsed = now - workingTimeStart; 
        const secondsElapsed = Math.floor(timeElapsed / 1000); 
        const minutesElapsed = Math.floor(secondsElapsed / 60);
        const hoursElapsed = Math.floor(minutesElapsed / 60); 
        workingTimeDisplay.innerHTML = `${hoursElapsed} hours <br>${minutesElapsed%60} minutes <br>${secondsElapsed%60} seconds`;
    }
    else {
        workingTimeDisplay.innerHTML = "0 hours <br>0 minutes<br>0 seconds"; 
    }
}

function updatePauseTime() {
    if (currentlyWorking) {
        pauseTime = Math.floor((timeElapsed/1000) * ratio); 
        pauseTimeDisplay.style.color = "black"; 
        pauseTimeDisplay.innerHTML = `${Math.floor(pauseTime/3600)} hours <br>${Math.floor(pauseTime/60)%60} minutes <br>${pauseTime%60} seconds`;

    }
    else {
        if (pauseTime > 0) {
            pauseTimeDisplay.style.color = "black"; 
            pauseTime--; 
            pauseTimeDisplay.innerHTML = `${Math.floor(pauseTime/3600)} hours <br>${Math.floor(pauseTime/60)%60} minutes <br>${pauseTime%60} seconds`;
        }
        else {
            pauseTimeDisplay.innerHTML = `${Math.floor(pauseTime/3600)} hours <br>${Math.floor(pauseTime/60)%60} minutes <br>${pauseTime%60} seconds`;
            pauseTimeDisplay.style.color = "red"; 
        }

    }
}

setInterval(updateWorkingTime, 1000); 
setInterval(updatePauseTime, 1000); 

startButton.addEventListener('click', startClock); 